import pytest
import time
from selenium.webdriver import Chrome
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager

from pages.login_page import LoginPage
from pages.cockpit_page import CockpitPage
from Util.random import RandomUtil
from pages.admin_page import AdminPage
from pages.add_project_page import AddProjectPage


@pytest.fixture()
def browser():
    browser = Chrome(executable_path=ChromeDriverManager().install())
    login_page = LoginPage(browser)
    login_page.load()
    login_page.login("administrator@testarena.pl", "sumXQQ72$L")
    yield browser
    browser.quit()


def test_open_administration(browser):
    cockpit_page = CockpitPage(browser)
    cockpit_page.click_administration()

    assert browser.find_element(By.CSS_SELECTOR, '.content_title').text == 'Projekty'


def test_add_project(browser):
    cockpit_page = CockpitPage(browser)
    cockpit_page.click_administration()

    admin_page = AdminPage(browser)
    admin_page.click_add_project()

    assert browser.find_element(By.CSS_SELECTOR, '.content_title').text == 'Dodaj projekt'


def test_save_new_project(browser):
    cockpit_page = CockpitPage(browser)
    cockpit_page.click_administration()

    admin_page = AdminPage(browser)
    admin_page.click_add_project()

    add_project_page = AddProjectPage(browser)
    random = RandomUtil
    name = random.get_random_string(6)
    prefix = random.get_random_string(8)
    add_project_page.create_and_save_new_project(name, prefix)

    # time.sleep(3)

    creation_date = browser.find_elements(By.CSS_SELECTOR, '.textLabelEditor_text')
    assert creation_date[5].text == 'DATA UTWORZENIA'


def test_find_new_saved_project(browser):
    cockpit_page = CockpitPage(browser)
    cockpit_page.click_administration()

    admin_page = AdminPage(browser)
    admin_page.click_add_project()

    add_project_page = AddProjectPage(browser)
    random = RandomUtil
    name = random.get_random_string(8)
    prefix = random.get_random_string(6)
    time.sleep(1)
    add_project_page.create_and_save_new_project(name, prefix)
    time.sleep(1)
    add_project_page.click_projects()

    time.sleep(1)
    search_field = browser.find_element(By.CSS_SELECTOR, '#search')
    search_field.send_keys(name)
    search_button = browser.find_element(By.CSS_SELECTOR, '#j_searchButton')
    search_button.click()
    time.sleep(1)

    found_project_name = browser.find_elements(By.CSS_SELECTOR, "a")
    assert found_project_name[13].text == name
