from selenium.webdriver.common.by import By


class AddProjectPage:
    def __init__(self, browser):
        self.browser = browser

    def create_and_save_new_project(self, name, prefix):
        self.browser.find_element(By.CSS_SELECTOR, "#name").send_keys(name)
        self.browser.find_element(By.CSS_SELECTOR, "#prefix").send_keys(prefix)
        self.browser.find_element(By.CSS_SELECTOR, "#save").click()

    def click_projects(self):
        self.browser.find_element(By.CSS_SELECTOR, ".activeMenu").click()